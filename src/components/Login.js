import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import Layout from "./layout/Layout";

// importing styles
import "./Login.css";

// importing reducers
import { toggle } from "../features/login/loginSlice";

const Login = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [isWrongInput, setIsWrongInput] = useState(false);

  const history = useHistory();

  const dispatch = useDispatch();

  const updateUserNameInput = (e) => {
    setUserName(e.target.value);
  };

  const updatePasswordInput = (e) => {
    setPassword(e.target.value);
  };
  const toggleLoginState = (e) => {
    e.preventDefault();

    if (userName === "admin" && password === "admin") {
      dispatch(toggle());
      history.push("/");
    } else {
      setIsWrongInput(!isWrongInput);
      setTimeout(() => {
        setIsWrongInput(false);
      }, 3000);
    }
  };
  return (
    <Layout>
      <div className="login-form-container">
        {isWrongInput && (
          <span className="login-form-warning">Wrong Input!</span>
        )}
        <form className="login-form">
          <h1 className="login-form__header">Login</h1>
          <div className="form-group">
            <label htmlFor="login-form__user-name">Username</label>
            <input
              placeholder="Enter username"
              type="text"
              id="login-form__user-name"
              value={userName}
              onChange={updateUserNameInput}
            />
          </div>
          <div className="form-group">
            <label htmlFor="login-form__password">Password</label>
            <input
              placeholder="Enter password"
              type="password"
              id="login-form__password"
              value={password}
              onChange={updatePasswordInput}
            />
          </div>
          <div>
            <button
              type="submit"
              onClick={toggleLoginState}
              className="login-form__submit"
            >
              Login
            </button>
          </div>
        </form>
      </div>
    </Layout>
  );
};

export default Login;
