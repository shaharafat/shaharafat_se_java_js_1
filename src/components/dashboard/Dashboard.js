import React, { useEffect } from "react";
import Layout from "../layout/Layout";

import { useSelector, useDispatch } from "react-redux";
import { addTopProduct } from "../../features/product/topProductSlice";

// importing styles
import "./Dashboard.css";
import DashBoardListItem from "./DashBoradListItem";

const Dashboard = () => {
  let dispatch = useDispatch();
  let products = useSelector((state) => state.products);
  let topProducts = useSelector((state) => state.topProduct.value);
  useEffect(() => {
    let topProducts = products.map((product) => product);
    console.log(topProducts);
    topProducts = topProducts
      .sort((a, b) => b.profitPercentage - a.profitPercentage)
      .slice(0, 5);

    dispatch(addTopProduct(topProducts));
  }, []);

  return (
    <div>
      <Layout>
        <h1>Dashboard</h1>
        <div className="dashboard-container">
          <div className="profitable-products">
            <h2 className="profitable-products__title">Profitable Products</h2>
            <div className="profitable-products__items">
              {topProducts.map((product) => (
                <DashBoardListItem {...product} />
              ))}
            </div>
          </div>

          <div>
            <h2>Sold products</h2>
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Dashboard;
