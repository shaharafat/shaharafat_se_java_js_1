import React from "react";

// importing styles
import "./DashBoardListItem.css";

const DashBoardListItem = ({ name, profitPercentage, type, price }) => {
  return (
    <div className="dashboard-item">
      <p className="dashboard-item__name">{name}</p>
      <div className="dashboard-item__info">
        <p className="dashboard-item__percentage">Profit: {profitPercentage}%</p>
        <div>
          <p className="dashboard-item__price">Product Type: {type}</p>
          <p className="dashboard-item__type">price: {price} BDT</p>
        </div>
      </div>
    </div>
  );
};

export default DashBoardListItem;
