import React from "react";
import Layout from "../layout/Layout";
import './About.css'
const About = () => {
  return (
    <div>
      <Layout className="about-page__container">
        <div className="about-page__content">
          <h1 className="about-page__title">About</h1>
          <p className="about-page__text">
            We are IT Bazar. Your first choice for online shopping. We ensure
            the best service along Bangladesh.
          </p>
        </div>
      </Layout>
    </div>
  );
};

export default About;
