import React from "react";
import Header from "./Header";
import Sidebar from "../sidebar/Sidebar"
import Footer from "./Footer";

// importing styles
import "./Layout.css";

const Layout = (props) => {
  return (
    <div className="layout-container">
      <Header />
      <Sidebar />
      <div className="layout-container__content">{props.children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
