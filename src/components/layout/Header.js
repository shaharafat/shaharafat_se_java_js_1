import React from "react";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";

// importing styles
import './Header.css'
const Header = () => {
  return (
    <div className="site-header">
      <span className="site-header__logo">
        <Link className="site-header__logo--link" to="/">IT Bazar</Link>
      </span>
      <Navbar />
    </div>
  );
};

export default Header;
