import React from "react";
import { Link } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { toggle } from "../../features/login/loginSlice";

// importing css
import "./Navbar.css";

const Navbar = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.login.value);
  return (
    <nav>
      <ul className="nav-bar">
        {isLoggedIn ? (
          <li className="nav-bar__item nav-bar__button">
            <Link
              className="nav-bar__item__link"
              onClick={() => dispatch(toggle())}
            >
              <i className="fas fa-sign-out-alt nav-bar__item__icon"></i>
              Logout
            </Link>
          </li>
        ) : (
          <li className="nav-bar__item nav-bar__button">
            <Link to="/login" className="nav-bar__item__link">
              <i className="fas fa-sign-in-alt nav-bar__item__icon"></i>
              Login
            </Link>
          </li>
        )}
        <li className="nav-bar__item">
          <Link to="/about" className="nav-bar__item__link">
            About
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
