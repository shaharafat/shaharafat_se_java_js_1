import React from "react";

import './ProductList.css'

const ProductList = ({id, name, profitPercentage, type, price,deleteProduct }) => {
  return (
    <div className="productList-item">
      <p className="productList-item__name">{name}</p>
      <div className="productList-item__info">
        <p className="productList-item__percentage">
          Profit: {profitPercentage}%
        </p>
        <div>
          <p className="productList-item__price">Product Type: {type}</p>
          <p className="productList-item__type">price: {price} BDT</p>
        </div>
        <button onClick={() => deleteProduct(id)} className="productList-item__delete">Delete Product</button>
      </div>
    </div>
  );
};

export default ProductList;
