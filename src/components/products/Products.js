import React from "react";
import { useSelector, useDispatch } from "react-redux";

import ProductBox from "./ProductBox";

import './Products.css'

const Products = () => {
  const products = useSelector((state) => state.products);
  return (
    <div className="products-container">
      {products.map((product) => (
        <ProductBox key={product.id} {...product} />
      ))}
    </div>
  );
};

export default Products;
