import React from "react";
import { useDispatch, useSelector } from 'react-redux'
import {deleteProduct} from '../../features/product/productSlice'
import ProductList from './ProductList'

import Layout from "../layout/Layout";
// importing styles
import "./DeleteProduct.css";

const DeleteProduct = () => {
  const products = useSelector(state => state.products)
  const dispatch = useDispatch();
  const deleteProductFromList = (id) => {
    console.log('delete...');
    dispatch(deleteProduct(id))
  }

  return (
    <div>
      <Layout>
        <h2>Delete Product</h2>
        <div className="productList-container">
          {
            products.map(product => <ProductList {...product} key={product.id} deleteProduct={deleteProductFromList} />)
          } 
        </div>
      </Layout>
    </div>
  );
};

export default DeleteProduct;
