import React from "react";

import "./ProductBox.css";

const ProductBox = ({ name, image, price, inStock }) => {
  return (
    <div className="product-box">
      <img
        src={image}
        className="product-box__product-image"
        alt="product image"
      />
      <div className="product-box__product-info">
        <p className="product-box__product-name">{name}</p>
        <div className="product-box__product-price-info">
          <p className="product-box__product-price">{price} BDT</p>
          <p className="product-box__available-product">
            {inStock ? <span>{inStock} in stock</span> : <span>Sold Out</span>}
          </p>
        </div>
      </div>
    </div>
  );
};

export default ProductBox;
