import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import Layout from "../layout/Layout";
import { addProduct } from "../../features/product/productSlice";
// importing styles
import "./AddProduct.css";

const AddProduct = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [price, setPrice] = useState(0);
  const [inStock, setInStock] = useState(0);
  const [profitPercentage, setProfitPercentage] = useState(0);
  const [isValid, setIsValid] = useState(true);
  const addNewProduct = (e) => {
    e.preventDefault();
    if (
      /^[a-zA-Z0-9]+$/.test(name) &&
      /^[a-zA-Z]+$/.test(type) &&
      /^[0-9]+$/.test(price) &&
      price > 1000 &&
      /^[0-9]+$/.test(inStock) &&
      /^[0-9]+$/.test(profitPercentage)
    ) {
      dispatch(addProduct(name, price, profitPercentage, type, inStock));
      history.push("/");
    } else {
      setIsValid(!isValid);

      setTimeout(() => setIsValid(true), 3000);
    }
  };

  return (
    <div>
      <Layout>
        <h2>Add Product</h2>
        <div className="new-product">
          {isValid || (
            <span
              style={{
                color: "red",
                padding: ".5rem 2rem",
                backgroundColor: "white",
                margin: "0 auto",
              }}
            >
              Wrong Input!{" "}
            </span>
          )}
          <h2 className="new-product__form-title login-form__header">
            Type Product Information
          </h2>
          <form className="new-product__form">
            <div className="form-group">
              <label htmlFor="new-product__name">Product Name</label>
              <input
                placeholder="Enter product name"
                type="text"
                id="new-product__name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="new-product__type">Product Type</label>
              <input
                placeholder="Enter product type"
                type="text"
                id="new-product__type"
                value={type}
                onChange={(e) => setType(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="new-product__price">Product Price</label>
              <input
                placeholder="Enter product price"
                type="text"
                id="new-product__price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="new-product__inStock">Product Quantity</label>
              <input
                placeholder="Enter product quantity"
                type="text"
                id="new-product__inStock"
                value={inStock}
                onChange={(e) => setInStock(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="new-product__profitPercentage">
                Profit Percentage
              </label>
              <input
                placeholder="Enter profit percentage"
                type="text"
                id="new-product__profitPercentage"
                value={profitPercentage}
                onChange={(e) => setProfitPercentage(e.target.value)}
              />
            </div>
            <button className="login-form__submit" onClick={addNewProduct}>
              Add
            </button>
          </form>
        </div>
      </Layout>
    </div>
  );
};

export default AddProduct;
