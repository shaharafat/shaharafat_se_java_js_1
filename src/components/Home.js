import React from "react";
import Layout from "./layout/Layout";
import Product from "./products/Products"

const Home = () => {
  return (
    <div>
      <Layout>
        <Product/>
      </Layout>
    </div>
  );
};

export default Home;
