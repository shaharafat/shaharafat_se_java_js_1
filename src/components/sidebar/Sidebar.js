import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from 'react-redux'

// importing styles
import "./Sidebar.css";

const Sidebar = () => {
  const [sidebarActive, setSidebarActive] = useState(false);
  const isLoggedIn = useSelector(state => state.login.value);

  const toggleActive = () => {
    setSidebarActive(!sidebarActive);
  };

  return (
    isLoggedIn && 
    <nav className="sidebar-container">
      <ul className={`sidebar ${sidebarActive ? "" : "inactive"}`}>
        {sidebarActive ? (
          <li className="sidebar__toggle-item">
            <span className="sidebar__toggle-icon" onClick={toggleActive}>
              <i class="fas fa-caret-square-left"></i>
            </span>
          </li>
        ) : (
          <li className="sidebar__toggle-item">
            <span className="sidebar__toggle-icon" onClick={toggleActive}>
              <i class="fas fa-caret-square-right"></i>
            </span>
          </li>
        )}
        <li className="sidebar__item">
          <Link to="/dashboard" className="sidebar__item__link">
            Dashboard
          </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/addProduct" className="sidebar__item__link">
            Add Product
          </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/deleteProduct" className="sidebar__item__link">
            Delete Product
          </Link>
        </li>
        <li className="sidebar__item">
          <Link to="/updateProduct/:id" className="sidebar__item__link">
            Update Product
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Sidebar;
