import { createSlice } from '@reduxjs/toolkit'

const loginSlice = createSlice({
  name: 'login',
  initialState: {
    value: false
  },
  reducers: {
    toggle(state,action){
      state.value = !state.value;
      console.log(state.value);
    }
  }
})

export const { toggle } = loginSlice.actions;
export default loginSlice.reducer;