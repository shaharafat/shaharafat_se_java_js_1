import { createSlice, nanoid } from "@reduxjs/toolkit";

import data from "../../data";
const initialState = data;

const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addProduct: {
      reducer(state, action) {
        state.unshift(action.payload); // mutating using unshift,push,pop is not wrong. redux use immer library does all the thing internally. it will not mutate. it will create new state and set it for state value.
      },
      prepare(name, price, profitPercentage, type, inStock) {
        return {
          payload: {
            id: nanoid(),
            image: "http://dummyimage.com/230x240.jpg/ff4444/ffffff",
            name,
            price,
            profitPercentage,
            type,
          },
        };
      },
    },

    deleteProduct: {
      reducer(state, action) {
        state.splice(action.payload,1)
      },
    }
  },
});

export const { addProduct,deleteProduct } = productSlice.actions;
export default productSlice.reducer;
