import { createSlice } from "@reduxjs/toolkit";

const topProductSlice = createSlice({
  name: "topProduct",
  initialState: {
    value: [],
  },
  reducers: {
    addTopProduct(state, action) {
      state.value = action.payload;
    },
  },
});

export const { addTopProduct } = topProductSlice.actions;
export default topProductSlice.reducer;
