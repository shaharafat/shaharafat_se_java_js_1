import { configureStore } from '@reduxjs/toolkit';
import loginReducer from '../features/login/loginSlice'
import productRuducer from '../features/product/productSlice'
import topProductReducer from "../features/product/topProductSlice"

export default configureStore({
  reducer: {
    login: loginReducer,
    products: productRuducer,
    topProduct: topProductReducer,
  },
});
