import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import About from "./components/about/About";
import Dashboard from "./components/dashboard/Dashboard"
import AddProduct from "./components/products/AddProduct"
import DeleteProduct from "./components/products/DeleteProduct"
import UpdateProduct from "./components/products/UpdateProduct"

// importing styles
import "./App.css";

function App() {
  return (
    <div className="">
      <Router>
        <Switch>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/dashboard">
            <Dashboard/>
          </Route>
          <Route exact path="/addProduct">
            <AddProduct/>
          </Route>
          <Route exact path="/deleteProduct">
            <DeleteProduct/>
          </Route>
          <Route exact path="/updateProduct/:id">
            <UpdateProduct/>
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
